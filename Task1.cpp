// C++ program for implementation of Bisection Method for
// solving equations
#include<bits/stdc++.h>
#include<math.h>
using namespace std;

 
// An example function whose solution is determined using
// Bisection Method. The function is x^3 - x^2  + 2
double func(double x)
{
    return sin(x*x);
}
 
// Prints root of func(x) with error of EPSILON
void bisection(double a, double b, double ep)
{
    if (func(a) * func(b) >= 0)
    {
        cout << "You have not assumed right a and b\n";
        return;
    }
 
    double c = a;
    int n = 0;
    while ((b-a) >= ep)
    {
    
        // Find middle point
        c = (a+b)/2;
 
        // Check if middle point is root
        if (func(c) == 0.0)
        {
//        	cout<<"\n root point = "<<c;
        	break;
		}
		
            
 
        // Decide the side to repeat the steps
        else if (func(c)*func(a) < 0)
            b = c;
        else
            a = c;
        n++;    
    }
    cout << "The value of root is : " << c;
    cout<<"\n f(x0) ="<<func(c);
    cout<<"\nThe number of iteration it takes ="<<n;
}
 
// Driver program to test above function
int main()
{
	char cho;
	do{
		
		double epsilon,a,b;
		cout<<"Enter the value of epsilon =";
		cin>>epsilon;
		cout<<"Enter the value of a =";
		cin>>a;
		cout<<"Enter the value of b =";
		cin>>b;
		
	    // Initial values assumed
	    bisection(a, b,epsilon);
	    
	    cout<<"\n\n\n To quit enter q or Q :";
	    cin>>cho;
	}while(cho !='q'||cho !='Q');
    return 0;
}

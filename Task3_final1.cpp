#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <cstdlib>
//#include<conio.h>
using namespace std;

int jdate(int month, int day, int year);
struct Data
{
	int date;
	int month;
	int year;
	int julianDate;
	int max;
	int min;
};

int main() {
	vector<Data> dataArray;
  ifstream inputFile("SevilleTemp1960-2020.txt");
  string tempLine;
  getline(inputFile, tempLine, '\n');
  while ( getline(inputFile, tempLine, '\n') ) {

	stringstream a(tempLine);
	string i;
//	cout<<tempLine<<endl;
	Data d;
	getline(a,i,'\t');
	if(i=="")
	{
		getline(inputFile,tempLine,'\n');
//		cout<<"*********************************\n";
		continue;
	}
	string dateWhole = i;
//	cout<<i<<endl;
	stringstream dw(dateWhole);
	string tdw;
	getline(dw,tdw,'-');
	stringstream tdate(tdw);
	tdate>>d.date;
	getline(dw,tdw,'-');
	stringstream tmonth(tdw);
	tmonth>>d.month;
	getline(dw,tdw,'-');
	stringstream tyear(tdw);
	tyear>>d.year;
	d.julianDate = jdate(d.month,d.date,d.year);

	getline(a,i,'\t');
	stringstream str(i);
	str>>d.max;
	getline(a,i,'\t');
	stringstream str1(i);
	str1>>d.min;
	dataArray.push_back(d);

  }  
	ofstream outdata;
	outdata.open("Tempreature.csv"); // opens the file
   	if( !outdata ) 
	{ // file couldn't be opened
      cerr << "Error: file could not be opened" << endl;
      exit(1);
   	}
   	
    for (unsigned i = 0; i<dataArray.size(); i++)
    {
    	Data tempData = dataArray[i];
    	outdata << tempData.date<<","<<tempData.month<<","<<tempData.year<<","<<tempData.julianDate<<","<<tempData.max<<","<<tempData.min<< endl;
    	cout << tempData.date<<","<<tempData.month<<","<<tempData.year<<","<<tempData.julianDate<<","<<tempData.max<<","<<tempData.min<< endl;
	}
	outdata.close();
    //getch();
    return 0; 
}

//Function that calculates and displays julian dates and difference
int jdate(int month, int day, int year)
{
		long intRes1;
		long intRes2;
		long intRes3;
	   int jdn;
		double dayofweek;
   
	   
		//Calculates first julian date
		intRes1 = ((2 - year / 100) + (year / 400));
		intRes2 = (int)(365.25 * year);
		intRes3 = (int)(30.6001 * (month + 1));
		jdn = intRes1 + intRes2 + intRes3 + day + 1720994.5;
		dayofweek = (jdn + 1) % 7;
		return jdn;

}//Ends Function


